### Project Information
---
In this app shortest path is searched in the maze using breadth first search implemented with lee algorithm.  
SFML library is used to bring the visualisation of how algorithm works step by step.

There are two versions: one with orthogonal path and second with ortho-diagonal path.

The application provides measuring the running time of the algorithm in two ways:  
algorithm working time, and real time which takes into consideration step by step rendering of wave propagation.