#include <SFML/Graphics.hpp>
#include <iostream>

const int mHeight = 20;			//40x20 = 1280x640 pxl;
const int mWidth = 40;

const int Hedge_Tile = 999;
const int Free_Tile = 900;
const int Finish_Tile = 988;	//Start_tile = 0 

const int Tile_Size = 32;		//pxl	

sf::String mazeMap[mHeight] = {
	"****************************************",
	"*                         *            *",
	"*   *       ***                        *",
	"*      *         *                     *",
	"*                *                     *",
	"*   *     ***           *     *        *",
	"*   *       *          *      ****     *",
	"*   *             *    *       *       *",
	"*   *   *         *          **        *",
	"*   *   *         *           *      ***",
	"*   *                         ***    * *",
	"*   *        *   *     *           *** *",
	"*   *            *                 *   *",
	"*   *      *      **                 ***",
	"*   * *          *****       *         *",
	"*   *      *                 *         *",
	"*   *        *       **      *         *",
	"*   * **     *               ***       *",
	"*   *  *                       *       *",
	"****************************************"
};


inline int isFreeToStep(int** mat, int row, int col, int count, int rowfin, int colfin)
{
	if ((row >= 0) && (row < mHeight) && (col >= 0) && (col < mWidth)	//Проверка выхода за границы
		&& (mat[row][col] >= count)										//на заполненность предыдущим шагом
		&& (mat[row][col] != Hedge_Tile)								//на отсутствие препятствия
		&& (mat[rowfin][colfin] == Free_Tile)) return 1;

	else if (mat[rowfin][colfin] < mHeight * mWidth) return 2;			//Дошли до финальной ячейки

	else return 0;
}


void updateMatrix(int** mat, sf::Sprite& mapSprite, sf::RenderWindow& window, bool showNumSteps = true)
{
	bool alreadyDrawn = false;

	for (int i1 = 0; i1 < mHeight; i1++) {
		for (int j1 = 0; j1 < mWidth; j1++) {

			if (mat[i1][j1] == Hedge_Tile)			mapSprite.setTextureRect(sf::IntRect(0 * Tile_Size, 0, Tile_Size, Tile_Size));
			else if (mat[i1][j1] == Free_Tile)		mapSprite.setTextureRect(sf::IntRect(1 * Tile_Size, 0, Tile_Size, Tile_Size));
			else if (mat[i1][j1] == 0)				mapSprite.setTextureRect(sf::IntRect(3 * Tile_Size, 0, Tile_Size, Tile_Size));
			else if (mat[i1][j1] == Finish_Tile)	mapSprite.setTextureRect(sf::IntRect(4 * Tile_Size, 0, Tile_Size, Tile_Size));

			else if (mat[i1][j1] < 0) {										//Шаг найденного пути
																			//Отрисовка 2 раза: задний фон и число
				mapSprite.setPosition(j1 * Tile_Size, (i1 * Tile_Size));
				mapSprite.setTextureRect(sf::IntRect(2 * Tile_Size, 0, Tile_Size, Tile_Size));
				window.draw(mapSprite);
				if (showNumSteps) {
					int sec_row = ((int((mat[i1][j1]) * -1) / 10) + 1) * Tile_Size;
					int sec_col = (((mat[i1][j1]) * -1) % 10) * Tile_Size;

					mapSprite.setTextureRect(sf::IntRect(sec_col, sec_row, Tile_Size, Tile_Size));
					window.draw(mapSprite);
				}
				alreadyDrawn = true;
			}

			else {															//Шаг волны ищущей путь
																			//Отрисовка 2 раза: задний фон и число
				mapSprite.setPosition(j1 * Tile_Size, (i1 * Tile_Size));
				mapSprite.setTextureRect(sf::IntRect(1 * Tile_Size, 0, Tile_Size, Tile_Size));
				window.draw(mapSprite);

				if (showNumSteps) {
					int sec_row_counter = ((int(mat[i1][j1]) / 10) + 1) * Tile_Size;
					int sec_col_counter = ((mat[i1][j1]) % 10) * Tile_Size;

					mapSprite.setTextureRect(sf::IntRect(sec_col_counter, sec_row_counter, Tile_Size, Tile_Size));
					window.draw(mapSprite);
				}
				alreadyDrawn = true;
			}

			if (!alreadyDrawn) {
				mapSprite.setPosition(j1 * Tile_Size, i1 * Tile_Size);
				window.draw(mapSprite);
			}

			alreadyDrawn = false;
		}
	}
	return;
}



int main()
{
	setlocale(LC_ALL, "rus");

	int** mat = new int* [mHeight];		//Объвление целочисленной матрицы
	for (int i = 0; i < mHeight; i++)
		mat[i] = new int[mWidth];

	for (int i = 0; i < mHeight; i++) {		//Заполнение целочисленной матрицы данными лабиринта
		for (int j = 0; j < mWidth; j++) {
			if (mazeMap[i][j] == '*') mat[i][j] = Hedge_Tile;
			if (mazeMap[i][j] == ' ') mat[i][j] = Free_Tile;
		}
	}


	sf::RenderWindow window(sf::VideoMode(mWidth * Tile_Size, mHeight * Tile_Size), "Shortest Path In The Maze: LM - Start, RM - Finish, Enter - Show path numbers");

	sf::Image	mapImage;	mapImage.loadFromFile("Textures\\Texture_w_Num_Transp.png");
	sf::Texture map;		map.loadFromImage(mapImage);
	sf::Sprite	mapSprite;	mapSprite.setTexture(map);

	int startRow, startCol, finalRow, finalCol;

	bool pathNums = false;
	bool executeLee = false;

	int xPos1 = 0, yPos1 = 0, xPos2 = 0, yPos2 = 0;

	sf::Event event;
	sf::Vector2i pixelPos;
	sf::Vector2f squarePos;

	while (window.isOpen())
	{

		pixelPos = sf::Mouse::getPosition(window);
		squarePos = window.mapPixelToCoords(pixelPos);


		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();

			if (event.type == sf::Event::KeyPressed) {

				if ((event.key.code == sf::Keyboard::Enter)) {

					if (pathNums == false) pathNums = true;
					else if (pathNums == true) pathNums = false;
				}
			}

			if (event.type == sf::Event::MouseButtonPressed) {

				if (event.key.code == sf::Mouse::Left) {

					xPos1 = squarePos.x;
					yPos1 = squarePos.y;
					std::cout << "Win Position X(start): " << xPos1 << " | X_Start_Coord: " << int(xPos1 / Tile_Size)
						<< "\nWin Position Y(start): " << yPos1 << " | Y_Start_Coord: " << int(yPos1 / Tile_Size) << std::endl;
				}

				if (event.key.code == sf::Mouse::Right) {

					xPos2 = squarePos.x;
					yPos2 = squarePos.y;

					std::cout << "Win_Position X(fin):   " << xPos2 << " | X_Coord_Fin: " << int(xPos2 / Tile_Size)
						<< "\nWin_Position Y(fin):   " << yPos2 << " | Y_Coord_Fin: " << int(yPos2 / Tile_Size) << std::endl;

					executeLee = true;
				}
			}
		}



		if (executeLee == true && xPos1 != 0 && yPos1 != 0 && xPos2 != 0 && yPos1 != 0) {

			startRow = int(yPos1 / Tile_Size);				//Вычисление начала и конца пути
			startCol = int(xPos1 / Tile_Size);
			finalRow = int(yPos2 / Tile_Size);
			finalCol = int(xPos2 / Tile_Size);

			if (mat[startRow][startCol] == Hedge_Tile || mat[finalRow][finalCol] == Hedge_Tile) {	//Handling on Hedge click error

				xPos1 = 0; yPos1 = 0;
				xPos2 = 0; yPos2 = 0;
				std::cout << "Hedge chosen, Choose the free tiles as start and finish\n\n";
				continue;
			}

			for (int i = 0; i < mHeight; i++) {		//Заполнение матрицы исходными данными лабиринта
				for (int j = 0; j < mWidth; j++) {
					if (mazeMap[i][j] == '*') mat[i][j] = Hedge_Tile;
					if (mazeMap[i][j] == ' ') mat[i][j] = Free_Tile;
				}
			}

			////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////////

			sf::Clock clock;
			sf::Time timeElapsed;

			clock.restart();

			mat[startRow][startCol] = 0;

			int row[] = { 0, 0, -1, 1 }, col[] = { 1, -1, 0, 0 };

			int count = 0, im_row_counter = Tile_Size, im_col_counter = 0, sec_row_counter = 0, sec_col_counter = 0;

			int q = 0;

			bool pathExist = true;

			while (pathExist) {

				pathExist = false;

				for (int i = 0; i < mHeight; i++) {
					for (int j = 0; j < mWidth; j++) {

						if (mat[i][j] == count) {

							for (int k = 0; k < 4; k++) {

								q = isFreeToStep(mat, i + row[k], j + col[k], count, finalRow, finalCol);

								if (q == 1) {
									mat[i + row[k]][j + col[k]] = count + 1;
									pathExist = true;
								}
								else if (q == 2) {
									pathExist = true;
									break;
								}
							}

							//window.clear();
							//updateMatrix(mat, mapSprite, window);    //Для вывода волны поэтапно
							//window.display();
							//sf::sleep(sf::milliseconds(30));
						}
						if (q == 2) break;
					}
					if (q == 2) break;
				}

				window.clear();
				updateMatrix(mat, mapSprite, window);		//Для вывода фронта волны
				window.display();
				sf::sleep(sf::milliseconds(30));

				if (q == 2) break;

				count++;
			}

			if (pathExist == false) {
				xPos1 = 0; yPos1 = 0;
				xPos2 = 0; yPos2 = 0;
				std::cout << "\n!!Deadlock was met!!, path don't exist!!\n\n";
				continue;
			}

			int backcount;
			int rowfin1 = finalRow, colfin1 = finalCol;

			for (backcount = mat[rowfin1][colfin1] - 1; backcount >= 0; backcount--) {

				for (int k = 0; k < 4; k++) {

					if (mat[rowfin1 + row[k]][colfin1 + col[k]] == backcount) {

						rowfin1 = rowfin1 + row[k];
						colfin1 = colfin1 + col[k];

						mat[rowfin1][colfin1] = backcount * -1;
						break;
					}
				}

				window.clear();
				updateMatrix(mat, mapSprite, window);		//Отображение пути
				window.display();
				sf::sleep(sf::milliseconds(30));
			}

			mat[finalRow][finalCol] = Finish_Tile;

			timeElapsed = clock.getElapsedTime();
			std::cout << "Путь найден\nНаименьшее количество шагов: " << count + 1 << std::endl;
			std::cout << "За время: " << timeElapsed.asMilliseconds() / 1000.0 << " сек\n\n";
			////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////////

			pathNums = true;

			executeLee = false;
		}


		if (pathNums == false) {

			window.clear();
			updateMatrix(mat, mapSprite, window, false);
			window.display();
		}


		if (pathNums == true) {

			window.clear();
			updateMatrix(mat, mapSprite, window);
			window.display();
		}

	}

	for (int i = 0; i < mHeight; i++)	//Удаление целочисленной матрицы
		delete[] mat[i];
	delete[] mat;

	return 0;
}